//
//  IdeasTableViewCell.swift
//  ideapool
//
//  Created by Arnaud Phommasone on 13/02/18.
//  Copyright © 2018 Arnaud Phommasone. All rights reserved.
//

import UIKit

protocol IdeasTableViewCellDelegate {
    func cellDidPickOptions (_from cell:IdeasTableViewCell, _at index: Int)
}


class IdeasTableViewCell: UITableViewCell {
    @IBOutlet weak var ideaTitleLabel: UILabel!
    @IBOutlet weak var impactLabel: UILabel!
    @IBOutlet weak var averageLabel: UILabel!
    @IBOutlet weak var easeLabel: UILabel!
    @IBOutlet weak var confidenceLabel: UILabel!
    @IBOutlet weak var ideaBackgroundView: UIView!
   
    var delegate:IdeasTableViewCellDelegate?
    
    // We store the index because it's more convenient when we will clikc on the button
    // for the options.
    var index: Int = -1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // We could put a shadow but we will need to :
        // Rasterize background so it will render a pixellised cell
        // Update Calayer when rotating view
//        ideaBackgroundView.layer.shadowColor = UIColor.black.cgColor
//        ideaBackgroundView.layer.shadowOpacity = 1
//        ideaBackgroundView.layer.shadowOffset = CGSize.zero
//        ideaBackgroundView.layer.shadowRadius = 1
//        ideaBackgroundView.layer.shadowPath = UIBezierPath(rect: ideaBackgroundView.bounds).cgPath
//        ideaBackgroundView.layer.shouldRasterize = true

        
        ideaBackgroundView.layer.shadowColor = UIColor.black.cgColor
        ideaBackgroundView.layer.shadowRadius = 1.0
        ideaBackgroundView.layer.shadowOpacity = 0.2
        ideaBackgroundView.layer.shadowOffset = CGSize(width: 4, height: 4)
        ideaBackgroundView.layer.masksToBounds = false
        
        ideaBackgroundView.layer.cornerRadius = 4
        
        ideaBackgroundView.layer.borderColor = UIColor.lightGray.cgColor
        ideaBackgroundView.layer.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func optionsAction(_ sender: Any) {
        self.delegate?.cellDidPickOptions(_from: self, _at: self.index)
    }
    

}
