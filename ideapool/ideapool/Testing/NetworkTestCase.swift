//
//  NetworkTestCase.swift
//  ideapoolTests
//
//  Created by Arnaud Phommasone on 10/02/18.
//  Copyright © 2018 Arnaud Phommasone. All rights reserved.
//

import XCTest
@testable import ideapool

class NetworkTestCase: XCTestCase {
    var loginAPIService: AuthentificationService! = nil
    var ideaAPIService: IdeaService! = nil
    var currentToken: Token? = nil
    
    var createdIdea: Idea? = nil
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        loginAPIService = AuthentificationService()
        ideaAPIService = IdeaService()
    }
    
    func testLogin() {
        loginAPIService.loginUser(email: "arnaud.phommasone@codementor.io", password: "the-Secret-123") { (token) in
            print("token \(token)")
            XCTAssertFalse(token.jwt == "" || token.refresh_token == "")
            self.currentToken = token
        }
    }
    
    func testRefreshToken() {
        if (self.currentToken != nil) {
            loginAPIService.refreshToken(token: currentToken!, completion: { (newToken) in
                XCTAssertTrue(newToken.refresh_token == self.currentToken?.refresh_token &&
                newToken.jwt != self.currentToken?.jwt )
            })
        }
    }
    
    func testCreateIdea() {
        if (self.currentToken != nil) {
            let creatingIdeaExpectation = expectation(description: "Creating a new idea")
            
            ideaAPIService.createIdea(token: currentToken!, content: "Test of creating a token", impact: 3, ease: 5, confidence: 5, completion: { (result) in
                
                if (result.isEmpty()) {
                    XCTFail("Failed to create a new idea. Probably parameters are not all filled")
                }
                creatingIdeaExpectation.fulfill()
                
                self.createdIdea = result
            })
        }
    }
    
    func testUpdatingIdea() {
        if (self.currentToken != nil) {
            let updatingIdeaExpectation = expectation(description: "Updating a new idea")
            let updateContent = "Updated Content"
            let updatedImpact = 2
            let updatedEase = 3
            let updatedConfidence = 4
            
            ideaAPIService.updateIdea(token: currentToken!, id: (self.createdIdea?.id)!, content: updateContent, impact: updatedImpact, ease: updatedEase, confidence: updatedConfidence, completion: { (result) in
                
                if (result.isEmpty()) {
                    XCTFail("Failed to update an idea. Probably parameters are not all filled")
                }
                
                if result.confidence == updatedConfidence &&
                    result.impact == updatedImpact &&
                    result.ease == updatedEase {
                    updatingIdeaExpectation.fulfill()
                }
            })
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
//    func testExample() {
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
