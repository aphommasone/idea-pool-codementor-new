//
//  Helper.swift
//  ideapool
//
//  Created by Arnaud Phommasone on 13/02/18.
//  Copyright © 2018 Arnaud Phommasone. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSAttributedStringKey.link, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
}

class IPTextField: UITextField {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let border = CALayer()
        let width = CGFloat(0.5)
        
        border.borderColor = Constants.Appearance.kTextFieldBottomBorderColour.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

class Helper: NSObject {
    class var sharedInstance: Helper {
        struct Static {
            static let instance: Helper = Helper()
        }
        return Static.instance
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func average(numbers: Int...) -> Double {
        var sum = 0
        for number in numbers {
            sum += number
        }
        let  ave : Double = Double(sum) / Double(numbers.count)
        return ave
    }
    
    func truncateAverage(average: Double) -> Double {
        return  Double(floor(100*average)/100)
    }

    func showErrorMessage(_on viewController: UIViewController) {
        // Show some alert saying credentials went wrong
        let failedAlert = UIAlertController(title: Constants.Alert.kFailedNetworkTitle, message: Constants.Alert.kFailedNetworkMessage, preferredStyle: .alert)
        failedAlert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (alertAction) in
            failedAlert.dismiss(animated: true, completion: nil)
        }))
        viewController.present(failedAlert, animated: true, completion: nil)
    }
    
    func customizeTextView(_from textView: UITextView, _with text: String, _and url: String) -> UITextView {
        let textVw = textView
        
        let yourstring = NSMutableAttributedString(string:text)
        let linkWasSet = yourstring.setAsLink(textToFind: url, linkURL: "somethingURL")
        
        if linkWasSet {
            // adjust more attributedString properties
        }
        
        // Make web links clickable
        textVw.isUserInteractionEnabled = true
        textVw.isSelectable = true
        textVw.isEditable = false
        textVw.dataDetectorTypes = UIDataDetectorTypes.link
        
        // Update UITextView content
        textVw.attributedText = yourstring
        
        // Customize UITextView
        textVw.linkTextAttributes = [ NSAttributedStringKey.foregroundColor.rawValue : Constants.Appearance.kMainGreenColour, NSAttributedStringKey.underlineStyle.rawValue : NSUnderlineStyle.styleNone.rawValue]
        
        return textVw
    }
    
    func createIdeaPoolNavigationTitleItem() -> UIView {
        // Let's create a customised titleView
        var logoAndTitleView = UIView(frame: CGRect(x: 0, y: 0, width: 270, height: 100))
        
        // Add logo
        let logo = UIImage(named: "navigationBarLogo")
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit // set imageview's content mode
        imageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        logoAndTitleView.addSubview(imageView)
        
        // Add the title next to it
        let titleLabel = UILabel(frame: CGRect(x: 50, y: 4, width: 130, height: 30))
        titleLabel.text = "The Idea Pool"
        titleLabel.font = UIFont(name: "Apple SD Gothic Neo", size: 18)!
        titleLabel.textColor = UIColor.white
        logoAndTitleView.addSubview(titleLabel)
        
        return logoAndTitleView
    }
}
