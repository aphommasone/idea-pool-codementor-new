//
//  MainViewController.swift
//  ideapool
//
//  Created by Arnaud Phommasone on 13/02/18.
//  Copyright © 2018 Arnaud Phommasone. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    let mainPresenter = MainPresenter(token: nil, userService: UserService(), ideaService: IdeaService(), authentificationService: AuthentificationService())
    var allIdeas: [Idea]? = nil
    
    @IBOutlet weak var ideasTableView: UITableView!
    @IBOutlet weak var gotIdeasView: UIView!
    
    var selectedIdea: Idea? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mainPresenter.attachView(view: self)
        
        // Need to hide back button to the login page (create a logout button instead)
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isHidden = false
        self.ideasTableView.separatorStyle = .none
        
        // Add a logout button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .done, target: self, action: #selector(MainViewController.logout))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
    }
    
    @objc func logout() {
        self.mainPresenter.logout()
    }

    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.mainPresenter.getIdeas()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func createIdeaAction(_ sender: Any) {
        self.performSegue(withIdentifier: Constants.Segue.kMainToCreateDetailSegue, sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segue.kMainToCreateDetailSegue {
            let detailViewController = segue.destination as! DetailViewController
            detailViewController.editingMode = .Create
            detailViewController.detailPresenter.setToken(token: self.mainPresenter.getToken()!)
        }
        else if segue.identifier == Constants.Segue.kMainToUpdateDetailSegue {
            let detailViewController = segue.destination as! DetailViewController
            detailViewController.editingMode = .Update
            detailViewController.currentIdea = self.selectedIdea
            detailViewController.detailPresenter.setToken(token:  self.mainPresenter.getToken()!)
        }
    }
    
    func udpateUI() {
        self.ideasTableView.isHidden = (self.allIdeas?.count == 0)
    }
}

// MARK: - TableView Delegate & Datasource
extension MainViewController: UITableViewDelegate,UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count: Int = (allIdeas != nil) ? (allIdeas?.count)! : 0
        return count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ideaCellIdentifier", for:indexPath) as! IdeasTableViewCell
        
        let idea = allIdeas![indexPath.row]
        
        cell.ideaTitleLabel.text = idea.content
        cell.confidenceLabel.text = String(idea.confidence)
        cell.easeLabel.text = String(idea.ease)
        cell.impactLabel.text = String(idea.impact)
        
        // Truncate double digit after coma
        cell.averageLabel.text = String(Helper.sharedInstance.truncateAverage(average: idea.average_score))
        
        cell.index = indexPath.row
        cell.delegate = self
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 132
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
}

extension MainViewController : MainPresenterView {
    
    func fetchingIdeasFound(ideas: [Idea]) {
        self.allIdeas = ideas
        self.ideasTableView.reloadData()
    }
    
}

extension MainViewController : IdeasTableViewCellDelegate {
    func cellDidPickOptions (_from cell:IdeasTableViewCell, _at index: Int) {
        // We will show the actionview. We could move it into a helper but because there's only one screen we probably can keep it into UIViewController
        let editOptions: UIAlertController = UIAlertController(title: "Actions", message: nil, preferredStyle: .actionSheet)
        let editAction = UIAlertAction(title: "Edit", style: .default) { (action) in
            self.selectedIdea = self.allIdeas?[index]
            self.performSegue(withIdentifier: Constants.Segue.kMainToUpdateDetailSegue, sender: self)
        }
        
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { (action) in
            
            let confirmAlert: UIAlertController = UIAlertController(title: "Are you sure ?", message: "This idea will be permanently deleted.", preferredStyle: .alert)
            let alertConfirmAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                // Remove the idea
                self.mainPresenter.deleteIdea(idea: self.allIdeas![index])
                
                // Add delete actions here
                self.allIdeas!.remove(at: index)
                
                // Create indexpath
                let indexPath = IndexPath(row: index, section: 0)
                self.ideasTableView.deleteRows(at: [indexPath], with: .fade)
                
                self.udpateUI()
            })
            let alertCancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                // Do something if needed
            })
            confirmAlert.addAction(alertConfirmAction)
            confirmAlert.addAction(alertCancelAction)
            
            self.present(confirmAlert, animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            // DO something if needed
            
        }
        
        editOptions.addAction(editAction)
        editOptions.addAction(deleteAction)
        editOptions.addAction(cancelAction)
        
        self.present(editOptions, animated: true, completion: nil)
    }
}
