//
//  DetailViewController.swift
//  ideapool
//
//  Created by Arnaud Phommasone on 13/02/18.
//  Copyright © 2018 Arnaud Phommasone. All rights reserved.
//

import UIKit

enum EditingMode: Int {
    case Update = 0
    case Create = 1
}

extension UIToolbar {
    
    func ToolbarPiker(mySelect : Selector) -> UIToolbar {
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: mySelect)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
}

class DetailViewController: UIViewController, UITextFieldDelegate{
    let detailPresenter = DetailPresenter(token: nil, ideaService: IdeaService())
    
    @IBOutlet weak var contentTexfield: UITextField!
    @IBOutlet weak var impactTexfield: UITextField!
    @IBOutlet weak var easeTexfield: UITextField!
    @IBOutlet weak var confidenceTextfield: UITextField!
    @IBOutlet weak var averageTextfield: UITextField!
    
    var editingMode:EditingMode = .Create
    var activeTextField: UITextField? = nil
    var pickerValue: UIPickerView = UIPickerView()
    var pickerDatasource: [Int] = Array(0...10)
    
    var currentIdea: Idea? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.pickerValue.delegate = self
        self.pickerValue.dataSource = self
        
        // We dont want the average to be written by user, but calculated automatically from other values
        averageTextfield.isEnabled = false
        
        self.addPicker(_to: confidenceTextfield)
        self.addPicker(_to: impactTexfield)
        self.addPicker(_to: easeTexfield)
        
        // Assign delegates
        contentTexfield.delegate = self
        impactTexfield.delegate = self
        confidenceTextfield.delegate = self
        easeTexfield.delegate = self
        
        // Change colours
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.currentIdea != nil {
            self.fillUI(idea: self.currentIdea!)
        }
        
        self.detailPresenter.attachView(view: self)
    }
    
    func fillUI(idea: Idea) {
        contentTexfield.text = idea.content
        confidenceTextfield.text = String(idea.confidence)
        impactTexfield.text = String(idea.impact)
        easeTexfield.text = String(idea.ease)
        
        // Truncate double digit after coma
        let averageValue = Helper.sharedInstance.truncateAverage(average: idea.average_score)
        
        averageTextfield.text = String(averageValue)
    }
    
    func addPicker(_to textField: UITextField) {
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(DetailViewController.dismissPicker))
        
        textField.inputAccessoryView = toolBar
        textField.inputView = self.pickerValue
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func dismissPicker() {
//        self.dobTextField.text = dateFormatterGet.string(from: datePicker.date)
        let selectedIndex =  self.pickerValue.selectedRow(inComponent: 0)
        let selectedValue = pickerDatasource[selectedIndex]
        self.activeTextField?.text = String(selectedValue)
        view.endEditing(true)
        
        // Update average
        let impactValue = Int(self.impactTexfield.text ?? "0")
        let easeValue = Int(self.easeTexfield.text ?? "0")
        let confidenceValue = Int(self.confidenceTextfield.text ?? "0")
        
        var averageValue = Helper.sharedInstance.average(numbers: impactValue ?? 0, easeValue ?? 0, confidenceValue ?? 0)
        averageValue = Double(floor(100*averageValue)/100)
        
        self.averageTextfield.text = String(averageValue)
    }
    
    // Remember the active textfield
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        // We just going back to the previous screen
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        let impactValue = Int(self.impactTexfield.text ?? "0")
        let easeValue = Int(self.easeTexfield.text ?? "0")
        let confidenceValue = Int(self.confidenceTextfield.text ?? "0")
        
        var averageValue = Helper.sharedInstance.average(numbers: impactValue ?? 0, easeValue ?? 0, confidenceValue ?? 0)
        
        // Truncate double digit after coma
        averageValue = Helper.sharedInstance.truncateAverage(average: averageValue)
        
        if self.editingMode == .Update {
            self.detailPresenter.update(idea: self.currentIdea!, newContent: self.contentTexfield.text ?? "", newImpactValue: impactValue!, newEaseValue: easeValue!, newConfidenceValue: confidenceValue!, newAverageValue: averageValue)
        }
        else if self.editingMode == .Create {
            self.detailPresenter.create(newContent: self.contentTexfield.text ?? "", newImpactValue: impactValue ?? 0, newEaseValue: easeValue ?? 0, newConfidenceValue: confidenceValue ?? 0, newAverageValue: averageValue)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}


extension DetailViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
          return 1
    }
    
    // The number of rows of data
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         return self.pickerDatasource.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(self.pickerDatasource[row])
    }
}
