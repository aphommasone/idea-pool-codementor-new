//
//  LoginViewController.swift
//  Idea Pool
//
//  Created by Arnaud Phommasone on 9/02/18.
//  Copyright © 2018 Arnaud Phommasone. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    let loginPresenter = LoginPresenter(authentificationService:AuthentificationService())
    var foundToken: Token? = nil
    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var dontHaveAccountLabel: UILabel!
    
    @IBOutlet weak var dontHaveAccountTextView: UITextView!
    
    @IBOutlet weak var emailTextFieldTopConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        // We need to attach the presenter here
        self.loginPresenter.attachView(view: self)
        
        // Setting the delagete
        passwordTextfield.delegate = self
        emailTextfield.delegate = self
    
        self.dontHaveAccountTextView.delegate = self
        self.dontHaveAccountTextView = Helper.sharedInstance.customizeTextView(_from: self.dontHaveAccountTextView, _with: "Don't have an account ? Create an account", _and: "Create an account")
        
        
        self.navigationController?.navigationBar.topItem?.titleView = Helper.sharedInstance.createIdeaPoolNavigationTitleItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.dontHaveAccountTextView.font = UIFont.systemFont(ofSize: 13)
        
        self.arrangeConstraintLayout()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.arrangeConstraintLayout()
    }
    
    func arrangeConstraintLayout() {
        if UIDevice.current.orientation.isLandscape {
            self.emailTextFieldTopConstraint.constant = 10
        }
        else {
            self.emailTextFieldTopConstraint.constant = 59
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Actions
    @IBAction func loginAction(_ sender: Any) {
        self.loginPresenter.login(email: self.emailTextfield.text!, password: self.passwordTextfield.text!)
    }
    
    // MARK: - Navigation
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == Constants.Segue.kLoginToMainSegue {
            let mainViewController = segue.destination as! MainViewController
            mainViewController.mainPresenter.setToken(token: foundToken!)
        }
    }
}

extension LoginViewController : UITextFieldDelegate, UITextViewDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        // Do someting
        self.loginPresenter.changeToSignup()
        return false
    }
}

extension LoginViewController : LoginPresenterView {
    func credentialsDidSucceeded(token: Token) {
        self.foundToken = token
        self.performSegue(withIdentifier: Constants.Segue.kLoginToMainSegue, sender: self)
    }
    
    func credentialsDidFail() {
        // Blank the textfields
        self.emailTextfield.text = ""
        self.passwordTextfield.text = ""
        
        Helper.sharedInstance.showErrorMessage(_on: self)
    }
}

