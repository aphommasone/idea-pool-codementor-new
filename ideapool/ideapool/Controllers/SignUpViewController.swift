//
//  SignUpViewController.swift
//  Idea Pool
//
//  Created by Arnaud Phommasone on 9/02/18.
//  Copyright © 2018 Arnaud Phommasone. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    let signUpPresenter = SignUpPresenter(authentificationService: AuthentificationService())
    var foundToken: Token? = nil
    
    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    
    @IBOutlet weak var haveAnAccountTextView: UITextView!
    
    @IBOutlet weak var titleLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var usernameTextFieldTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var labelBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // We need to attach the presenter here
        self.signUpPresenter.attachView(view: self)
        
        // Setting the delagete
        usernameTextfield.delegate = self
        passwordTextfield.delegate = self
        emailTextfield.delegate = self
        
        self.haveAnAccountTextView.delegate = self
        self.haveAnAccountTextView = Helper.sharedInstance.customizeTextView(_from: self.haveAnAccountTextView, _with: "Already have an account? Log in", _and: "Log in")
        
        self.navigationController?.navigationBar.topItem?.titleView = Helper.sharedInstance.createIdeaPoolNavigationTitleItem()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.haveAnAccountTextView.font = UIFont.systemFont(ofSize: 13)
        
        self.arrangeConstraintLayout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.arrangeConstraintLayout()
    }
    
    func arrangeConstraintLayout() {
        if UIDevice.current.orientation.isLandscape {
            self.usernameTextFieldTopConstraint.constant = 10
            self.titleLabelTopConstraint.constant = 10
            self.labelBottomConstraint.constant = 8
        }
        else {
            self.usernameTextFieldTopConstraint.constant = 96
            self.titleLabelTopConstraint.constant = 55
            self.labelBottomConstraint.constant = 33
        }
    }
    
    @IBAction func changeLoginAction(_ sender: Any) {
        self.signUpPresenter.changeToLogin()
    }
    
    @IBAction func signupAction(_ sender: Any) {
        self.signUpPresenter.signUp(email: self.emailTextfield.text!, password: self.passwordTextfield.text!, username: self.usernameTextfield.text!)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == Constants.Segue.kSignUpToMainSegue {
            let mainViewController = segue.destination as! MainViewController
            mainViewController.mainPresenter.setToken(token: foundToken!)
        }
    }

}


extension SignUpViewController : UITextFieldDelegate, UITextViewDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        // Do someting
        self.signUpPresenter.changeToLogin()
        return false
    }
}

extension SignUpViewController : SignUpPresenterView {
    func signUpDidSucceeded(token: Token) {
        self.foundToken = token
        self.performSegue(withIdentifier: Constants.Segue.kSignUpToMainSegue, sender: self)
    }
    
    func signUpDidFail() {
        
    }
}
