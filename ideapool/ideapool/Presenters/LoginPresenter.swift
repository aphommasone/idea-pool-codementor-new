//
//  LoginPresenter.swift
//  Idea Pool
//
//  Created by Arnaud Phommasone on 9/02/18.
//  Copyright © 2018 Arnaud Phommasone. All rights reserved.
//

import UIKit


protocol LoginPresenterView: NSObjectProtocol {
    func credentialsDidSucceeded(token : Token);
    func credentialsDidFail();
}

class LoginPresenter: NSObject {
    private let authentificationService:AuthentificationService
    weak private var loginView : LoginViewController?
    private var navigationController: UINavigationController? = nil
    
    init(authentificationService: AuthentificationService) {
        self.authentificationService = authentificationService
    }
    
    func attachView(view:LoginViewController) {
        loginView = view
        navigationController = loginView?.navigationController
    }
    
    func detachView() {
        loginView = nil
        navigationController = nil
    }
    
    
    func changeToSignup() {
        let signupViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants.Storyboard.kSignupVCIdentifier)
        
        self.navigationController?.setViewControllers([signupViewController], animated: false)
    }
    
    func login(email: String, password: String) {
        LoadingOverlay.shared.showOverlay()
        self.authentificationService.loginUser(email: email, password: password) { (resultToken) in
            print("Result token is \(resultToken)")
            if resultToken.isEmpty() {
                print("It's empty")
                self.loginView?.credentialsDidFail()
            }
            else {
                print("It succeeded")
                self.loginView?.credentialsDidSucceeded(token: resultToken)
            }
            LoadingOverlay.shared.hideOverlayView()
        }
    }
}
