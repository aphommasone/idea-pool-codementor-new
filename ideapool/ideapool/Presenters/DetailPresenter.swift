    //
//  DetailPresenter.swift
//  ideapool
//
//  Created by Arnaud Phommasone on 13/02/18.
//  Copyright © 2018 Arnaud Phommasone. All rights reserved.
//

import UIKit

class DetailPresenter: NSObject {
    private let ideaService:IdeaService
    weak private var detailView : DetailViewController?
    private var token: Token?

    init(token: Token?, ideaService: IdeaService) {
        self.token = token
        self.ideaService = ideaService
    }
    
    func attachView(view:DetailViewController) {
        detailView = view
    }
    
    func detachView() {
        detailView = nil
    }
    
    func setToken(token: Token) {
        self.token = token
    }
    
    func update(idea: Idea, newContent: String, newImpactValue: Int, newEaseValue: Int, newConfidenceValue: Int, newAverageValue: Double) {
        LoadingOverlay.shared.showOverlay()
        ideaService.updateIdea(token: self.token!, id: idea.id, content: newContent, impact: newImpactValue, ease: newEaseValue, confidence: newConfidenceValue, completion: { (updatedIdea) in
            print("result of the updated idea \(updatedIdea)")
            self.detailView?.navigationController?.popViewController(animated: true)
            LoadingOverlay.shared.hideOverlayView()
        })
    }
    
    func create(newContent: String, newImpactValue: Int, newEaseValue: Int, newConfidenceValue: Int, newAverageValue: Double) {
        LoadingOverlay.shared.showOverlay()
        ideaService.createIdea(token: self.token!, content: newContent, impact: newImpactValue, ease: newEaseValue, confidence: newConfidenceValue) { (idea) in
             print("result of the created idea \(idea)")
            
            if idea.isEmpty() {
                Helper.sharedInstance.showErrorMessage(_on: self.detailView!)
            }
            
            self.detailView?.navigationController?.popViewController(animated: true)
            LoadingOverlay.shared.hideOverlayView()
        }
    }
}
