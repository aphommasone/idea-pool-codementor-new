//
//  SignUpPresenter.swift
//  Idea Pool
//
//  Created by Arnaud Phommasone on 9/02/18.
//  Copyright © 2018 Arnaud Phommasone. All rights reserved.
//

import UIKit

protocol SignUpPresenterView: NSObjectProtocol {
    func signUpDidSucceeded(token : Token);
    func signUpDidFail();
}

class SignUpPresenter: NSObject {
    private let authentificationService:AuthentificationService
    weak private var signupView : SignUpViewController?
    private var navigationController: UINavigationController? = nil
    
    init(authentificationService: AuthentificationService) {
        self.authentificationService = authentificationService
    }
    
    func attachView(view:SignUpViewController) {
        signupView = view
        navigationController = signupView?.navigationController
    }
    
    func detachView() {
        signupView = nil
        navigationController = nil
    }
    
    func changeToLogin() {
        let loginViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants.Storyboard.kLoginVCIdentifier)

        self.navigationController?.setViewControllers([loginViewController], animated: false)
    }
    
    func signUp(email: String, password: String, username: String) {
        if Helper.sharedInstance.isValidEmail(testStr: email) {
            self.authentificationService.signUp(username: username, email: email, password: password) { (resultToken) in
                print("Result token is \(resultToken)")
                if resultToken.isEmpty() {
                    print("It's empty")
                    self.signupView?.signUpDidFail()
                }
                else {
                    print("It succeeded")
                    self.signupView?.signUpDidSucceeded(token: resultToken)
                }
            }
        }
        else {
            // Show error message
        }
    }
}
