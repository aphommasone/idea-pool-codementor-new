//
//  MainPresenter.swift
//  ideapool
//
//  Created by Arnaud Phommasone on 13/02/18.
//  Copyright © 2018 Arnaud Phommasone. All rights reserved.
//

import UIKit


protocol MainPresenterView: NSObjectProtocol {
    func fetchingIdeasFound(ideas: [Idea]);
}

class MainPresenter: NSObject {
    private let userService: UserService
    private let ideaService: IdeaService
    private let authentificationService: AuthentificationService
    private var token: Token?
    weak private var mainView : MainViewController?
    
    init(token: Token?, userService: UserService, ideaService: IdeaService, authentificationService: AuthentificationService) {
        self.token = token
        self.userService = userService
        self.ideaService = ideaService
        self.authentificationService = authentificationService
    }
    
    func setToken(token: Token) {
        self.token = token
    }
    
    func getToken() -> Token? {
        return token
    }
    
    func attachView(view:MainViewController) {
        mainView = view
    }
    
    func detachView() {
        mainView = nil
    }
    
    func getIdeas() {
        LoadingOverlay.shared.showOverlay()
        // Test retrieving everything
        ideaService.getIdea(token: self.token!, pages: 1, completion: { (allIdeas) in
            self.mainView!.fetchingIdeasFound(ideas: allIdeas)
             self.mainView!.udpateUI()
            LoadingOverlay.shared.hideOverlayView()
            
        })
    }
    
    func deleteIdea(idea: Idea) {
        LoadingOverlay.shared.showOverlay()
        ideaService.deleteIdea(token: self.token!, idIdea: idea.id) { (succeed) in
            if succeed == false {
                Helper.sharedInstance.showErrorMessage(_on: self.mainView!)
            }
            LoadingOverlay.shared.hideOverlayView()
        }
    }
    
    func logout() {
        LoadingOverlay.shared.showOverlay()
        authentificationService.logoutUser(token: self.token!) { (succeed) in
            LoadingOverlay.shared.hideOverlayView()
            self.mainView!.navigationController?.popToRootViewController(animated: true)
        }
    }
}
