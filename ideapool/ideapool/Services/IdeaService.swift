//
//  IdeaService.swift
//  ideapool
//
//  Created by Arnaud Phommasone on 10/02/18.
//  Copyright © 2018 Arnaud Phommasone. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper


enum IdeaRouter: URLRequestConvertible {
    static let baseURLString = Constants.Network.kBaseURL
    
    case createIdea(Token, [String: Any])
    case deleteIdea(Token, String)
    case getIdeas(Token, Int)
    case updateIdea(Token, String, [String: Any])
    
    func asURLRequest() throws -> URLRequest {
        var method: HTTPMethod {
            switch self {
            case .createIdea(let _, let _):
                return .post
            case .deleteIdea(let _, let _):
                return .delete
            case .getIdeas(let _, let _):
                return .get
            case .updateIdea(let _, let _, let _):
                return .put
            }
        }
        
        let params: ([String: Any]?) = {
            switch self {
            case .createIdea(let _, let idea):
                return idea
            case .deleteIdea(let _, let _):
                return nil
            case .getIdeas(let _, let _):
                return nil
            case .updateIdea(let _, let _, let idea):
                return idea
            }
        }()
        
        let url: URL = {
            // build up and return the URL for each endpoint
            let relativePath: String?
            switch self {
            case .createIdea(_, _):
                relativePath = Constants.Network.kIdeaCreateEndPoint
            case .deleteIdea(_, let idIdea):
                relativePath = "\(Constants.Network.kIdeaDeleteEndPoint)\(idIdea)"
            case .getIdeas(_, _):
                relativePath = Constants.Network.kIdeaGetEndPoint
            case .updateIdea(_, let idIdea, _):
                relativePath = "\(Constants.Network.kIdeaUpdateEndPoint)\(idIdea)"
            }
            
            var url = URL(string: LoginRouter.baseURLString)!
            if let relativePath = relativePath {
                url = url.appendingPathComponent(relativePath)
            }
            print("url : \(url)")
            return url
        }()
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        
        // Header fields
        
        switch self {
        case .createIdea(let token, _):
            urlRequest.addValue(token.jwt, forHTTPHeaderField: "X-Access-Token")
        case .deleteIdea(let token, let idIdea):
            urlRequest.addValue(token.jwt, forHTTPHeaderField: "X-Access-Token")
        case .getIdeas(let token, _):
            urlRequest.addValue(token.jwt, forHTTPHeaderField: "X-Access-Token")
        case .updateIdea(let token, let idIdea, let contentIdea):
            urlRequest.addValue(token.jwt, forHTTPHeaderField: "X-Access-Token")
        }
        
        let encoding = JSONEncoding.default
        return try encoding.encode(urlRequest, with: params)
    }
}

class IdeaService: NSObject {
    func createIdea(token: Token, content: String, impact: Int, ease: Int, confidence: Int, completion: @escaping (_ result: Idea) -> Void)-> () {
        let idea: [String: Any] = ["content": content, "impact": impact, "ease": ease, "confidence": confidence]
        let urlRequestProvider = IdeaRouter.createIdea(token, idea)
        Alamofire.request(urlRequestProvider).responseObject { (response: DataResponse<Idea>) in
            let idea = response.result.value
            completion(idea!)
        }
    }
    
    func updateIdea(token: Token, id: String, content: String, impact: Int, ease: Int, confidence: Int, completion: @escaping (_ result: Idea) -> Void)-> () {
        let idea: [String: Any] = ["content": content, "impact": impact, "ease": ease, "confidence": confidence]
        let urlRequestProvider = IdeaRouter.updateIdea(token, id, idea)
        Alamofire.request(urlRequestProvider).responseObject { (response: DataResponse<Idea>) in
            let idea = response.result.value
            completion(idea!)
        }
    }
    
    func deleteIdea(token: Token, idIdea: String, completion: @escaping (_ result: Bool) -> Void)-> () {
        let urlRequestProvider = IdeaRouter.deleteIdea(token, idIdea)
        
        Alamofire.request(urlRequestProvider)
            .responseJSON { response in
//                let resultat = String(data:  response.data!, encoding: .utf8)
                if let json = response.result.value as? [String: Any] {
                    print(json["reason"]!)
                    completion(false)
                }
                
                completion(true)
        }
    }
    
    func getIdea(token: Token, pages: Int, completion: @escaping (_ result: [Idea]) -> Void)-> () {
        let urlRequestProvider = IdeaRouter.getIdeas(token, pages)
        Alamofire.request(urlRequestProvider).responseJSON { (response) in
             let ideas = response.result.value
            
            var ideasResult: [Idea] = []
            if let array = response.result.value as? [[String: Any]] {
                // Parse object
                for idea in array {
                    let ideaObject = Idea(JSON: idea)
                    print("ideaObject \(ideaObject!)")
                    ideasResult.append(ideaObject!)
                }
            }
            completion(ideasResult)
        }
    }
}
